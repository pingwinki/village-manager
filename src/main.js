import Vue from 'vue'
import Vuelidate from 'vuelidate'
import VuelidateErrorExtractor, { templates } from 'vuelidate-error-extractor'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import App from './App.vue'

import { router } from './_helpers';

Vue.use(VuelidateErrorExtractor, {
  template: templates.singleErrorExtractor.foundaton6
})

import './assets/styles/tailwind.css'

Vue.config.productionTip = false

Vue.use(Vuelidate)
Vue.use(Vuex)
Vue.use(VueRouter)

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
