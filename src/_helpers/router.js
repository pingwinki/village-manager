import VueRouter from 'vue-router';
import HelloWorld from '../pages/HelloWorld.vue'

export const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: HelloWorld },
        { path: '*', redirect: '/' }
    ]
});
